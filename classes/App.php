<?php

namespace FSA\SmartHome;

use FSA\SmartHome\TTS\Queue;

abstract class App extends \FSA\Neuron\App
{
    private static $device_factory;
    private static $device_storage;
    private static $device_database;
    private static $sensor_storage;
    private static $sensor_database;
    private static $tts;
    private static $plugin;

    protected static function constVarPrefix(): string
    {
        return "shcc";
    }

    protected static function constSessionName(): string
    {
        return "shcc";
    }

    protected static function getContext(): ?array
    {
        return [
            'title' => 'SHCC',
            'dashboard' => self::getSettings('dashboard'),
            'session' => self::session()
        ];
    }

    public static function init()
    {
        parent::init();
        ini_set('syslog.filter', 'raw');
        openlog('shcc', LOG_PID | LOG_ODELAY, LOG_USER);
    }

    public static function deviceFactory(): DeviceFactory
    {
        if (is_null(self::$device_factory)) {
            self::$device_factory = new DeviceFactory(App::plugins()->get());
        }
        return self::$device_factory;
    }

    public static function deviceStorage(): DeviceStorage
    {
        if (is_null(self::$device_storage)) {
            self::$device_storage = new DeviceStorage(App::redis(), static::constVarPrefix() . ':devices');
        }
        return self::$device_storage;
    }

    public static function deviceDatabase(): DeviceDatabase
    {
        if (is_null(self::$device_database)) {
            self::$device_database = new DeviceDatabase(App::sql());
        }
        return self::$device_database;
    }

    public static function getDevice($uid)
    {
        $device = self::deviceDatabase()->get($uid);
        if ($device) {
            $object = self::deviceStorage()->get($device->plugin . ':' . $device->hwid);
            if ($object) {
                return $object;
            }
            return self::deviceFactory()->create($device->plugin, $device->hwid, $device->class, $device->properties);
        }
        return null;
    }

    public static function setDevice($uid, $object)
    {
        $device = self::deviceDatabase()->get($uid);
        if ($device) {
            return self::deviceStorage()->set($device->plugin . ':' . $device->hwid, $object);
        }
        return null;
    }

    public static function sensorStorage(): SensorStorage
    {
        if (is_null(self::$sensor_storage)) {
            self::$sensor_storage = new SensorStorage(App::redis(), static::constVarPrefix() . ':sensors');
        }
        return self::$sensor_storage;
    }

    public static function sensorDatabase(): SensorDatabase
    {
        if (is_null(self::$sensor_database)) {
            self::$sensor_database = new SensorDatabase(App::sql());
        }
        return self::$sensor_database;
    }

    public static function tts(): Queue
    {
        if (is_null(self::$tts)) {
            self::$tts = new Queue(App::redis(), static::constVarPrefix() . ':TTS');
        }
        return self::$tts;
    }

    public static function plugins(): Plugins
    {
        if (!isset(self::$plugin)) {
            self::$plugin = new Plugins;
        }
        return self::$plugin;
    }

    public static function storeEvents($uid, $events, $ts = null)
    {
        $sensor = self::sensorDatabase();
        foreach ($events as $property => $value) {
            $sensor->storeEvent($uid, $property, $value, $ts);
        }
    }

    public static function execEventsCustomScripts($uid, $events, $ts = null)
    {
        $custom_dir = __DIR__ . '/../custom/events/';
        if (!file_exists($custom_dir . $uid . '.php')) {
            return;
        }
        chdir($custom_dir);
        $eventsListener = require $uid . '.php';
        $eventsListener->uid = $uid;
        foreach ($events as $event => $value) {
            $method = 'on_event_' . str_replace('@', '_', $event);
            if (method_exists($eventsListener, $method)) {
                $eventsListener->$method($value, $ts);
            }
        }
    }

    public static function processEvents($plugin, $hwid, $events, $ts = null)
    {
        $uid = self::deviceDatabase()->searchUid($plugin, $hwid);
        if (!$uid) {
            return;
        }
        try {
            self::storeEvents($uid, $events, $ts);
        } catch (\Exception $ex) {
            syslog(LOG_ERR, 'Ошибка при сохранении данных с датчиков:' . PHP_EOL . $ex);
        }
        try {
            self::execEventsCustomScripts($uid, $events, $ts);
        } catch (\Exception $ex) {
            syslog(LOG_ERR, 'Ошибка при выполнении пользовательского скрипта events/' . $uid . '.php:' . PHP_EOL . $ex);
        }
    }

}
