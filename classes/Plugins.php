<?php

namespace FSA\SmartHome;

use Composer\InstalledVersions;

class Plugins
{
    const PLUGIN_INFO_FILENAME = 'shcc-plugin.json';

    private $plugins;

    private $query = [];

    public function __construct()
    {
        $this->plugins = $this->getBuiltInPlugins();
        $installed_packages = InstalledVersions::getInstalledPackages();
        foreach ($installed_packages as $package) {
            $path = InstalledVersions::getInstallPath($package) . DIRECTORY_SEPARATOR . self::PLUGIN_INFO_FILENAME;
            if (!file_exists($path)) {
                continue;
            }
            $json = file_get_contents($path);
            if ($json === false) {
                continue;
            }
            $plugin_info = json_decode($json);
            if (!$plugin_info) {
                continue;
            }
            if (!isset($plugin_info->name) or !isset($plugin_info->namespace) or !isset($plugin_info->description)) {
                continue;
            }
            $this->plugins[$plugin_info->name] = $plugin_info;
        }
        ksort($this->plugins);
    }

    public function getBuiltInPlugins(): array
    {
        return [
            "TTS" => (object)
            [
                "name" => "TTS",
                "namespace" => "FSA\\SmartHome\\TTS\\",
                "description" => "Синтезатор речи.",
                "daemon" => \FSA\SmartHome\TTS\Daemon::class,
                "settings" => [
                    'provider' => \FSA\SmartHome\TTS\Settings::getProvider(),
                    'pre_sound' => 'notification.mp3',
                    'pre_sound_period' => 5,
                    'play_sound_cmd' => 'mpg123 -q %s'
                ]
            ]
        ];
    }

    public function get(): array
    {
        return $this->plugins;
    }

    public function getPlugin($name) {
        if (isset($this->plugins[$name])) {
            return $this->plugins[$name];
        }
        return null;
    }

    public function query(): void
    {
        $this->query = $this->plugins;
    }

    public function fetchObject(): ?object
    {
        return array_shift($this->query);
    }

    public function rowCount(): int
    {
        return count($this->plugins);
    }
}
